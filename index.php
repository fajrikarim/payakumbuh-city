<?php
session_start();
include 'admin/query/conn.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Payakumbuh City</title>
    <?php include 'partials/head.php'; ?>
    <!-- as -->
</head>

<body>
    <!-- ##### Header Area Start ##### -->
    <?php include 'partials/header-area.php' ?>



    <?php include 'partials/modal.php' ?>



    <!-- ##### Featured Post Area Start ##### -->
    <div class="featured-post-area">
        <div class="container">
            <div class="hero-area row">
                <?php include 'partials/berita-utama.php' ?>
                <?php include 'partials/berita-lainnya.php' ?>
            </div>
        </div>
    </div>
    <!-- ##### Featured Post Area End ##### -->

    <!-- ##### Popular News Area Start ##### -->
    <div class="popular-news-area section-padding-10-20">
        <div class="container">
            <div class="row">
                <?php include 'partials/berita-terbaru.php' ?>


                <?php include 'partials/berita-populer.php' ?>

            </div>
        </div>
    </div>
    <!-- ##### Popular News Area End ##### -->


    <!-- ##### Editorial Post Area Start ##### -->
    <div class="editors-pick-post-area section-padding-80-50">
        <div class="container">
            <div class="row">
                <!-- Editors Pick -->
                <?php include 'partials/cerita-masyarakat.php' ?>

                <!-- World News -->
                <?php include 'partials/berita-internasional.php' ?>
            </div>
        </div>
    </div>

    <!-- ##### Footer Area Start ##### -->
    <?php include 'partials/footer.php' ?>

    <!-- Area Script -->
    <?php include 'partials/script.php' ?>
</body>

</html>