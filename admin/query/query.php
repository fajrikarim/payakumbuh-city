<?php
include 'conn.php';
if (isset($_POST['LogInAdmin'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $cek = $koneksi->query("SELECT * FROM tb_admin WHERE admin_username='$username' AND admin_password='$password'");
    $validasi = $cek->num_rows;

    if ($validasi >= 1) {
        $pecah = $cek->fetch_object();
        session_start();

        $_SESSION['admin'] = $pecah;
        echo "<script>
        alert('Selamat Datang, Anda Berhasil Login');
        window.location='../index.php';
        </script>";
    } else {
        echo "<script>
        alert('Username atau Password Anda Salah');
        window.location='../page/login.php'
        </script>";
    }
} elseif (isset($_GET['page']) == 'logout') {
    session_start();
    session_destroy();
    echo "<script>
        alert('Anda Berhasil Logout');
        window.location='../page/login.php'
        </script>";
} elseif (isset($_POST['tambahAdmin'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $nama = $_POST['nama'];
    $nohp = $_POST['nohp'];
    $ig = $_POST['ig'];

    $nama_foto = date('Ymd_His') . '_' . $_FILES['foto']['name'];
    $lokasi_foto = $_FILES['foto']['tmp_name'];

    move_uploaded_file($lokasi_foto, '../img/users/admin/' . $nama_foto);


    $simpan = $koneksi->query("INSERT INTO tb_admin VALUES('','$username','$password','$nama','$nohp','$ig','$nama_foto')");
    if ($simpan) {
        echo "<script>alert('Data Berhasil Disimpan');
        window.location='../index.php?page=page/data-admin';
        </script>";
    } else {
        echo "<script>alert('Data Gagal Disimpan');
        window.location='../index.php?page=page/data-admin';
        </script>";
    }
} elseif (isset($_POST['editAdmin'])) {
    $id = $_POST['adminId'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $nama = $_POST['nama'];
    $nohp = $_POST['nohp'];
    $ig = $_POST['ig'];

    if (empty($_FILES['foto']['name'])) {
        $edit = $koneksi->query("UPDATE tb_admin SET admin_username='$username',admin_password='$password',admin_nama='$nama',admin_nohp='$nohp',admin_ig='$ig', admin_foto='' where admin_id='$id'");
    } else {
        $nama_foto = date('Ymd_His') . '_' . $_FILES['foto']['name'];
        $lokasi_foto = $_FILES['foto']['tmp_name'];

        move_uploaded_file($lokasi_foto, '../img/users/admin/' . $nama_foto);

        $edit = $koneksi->query("UPDATE tb_admin SET admin_username='$username',admin_password='$password',admin_nama='$nama',admin_nohp='$nohp',admin_ig='$ig', admin_foto='$nama_foto' where admin_id='$id'");
    }


    if ($edit) {
        echo "<script>alert('Data Berhasil Di Edit');
        window.location='../index.php?page=page/data-admin';
        </script>";
    } else {
        echo "<script>alert('Data Gagal Di Edit');
        window.location='../index.php?page=page/data-admin';
        </script>";
    }
} elseif (isset($_GET['hapusAdmin'])) {
    $id = $_GET['hapusAdmin'];
    $queryAdmin = $koneksi->query("SELECT * FROM tb_admin where admin_id='$id'");
    $dataAdmin = $queryAdmin->fetch_object();

    $gambar = $dataAdmin->admin_foto;
    unlink('../img/users/admin/' . $gambar);
    $hapus = $koneksi->query("DELETE FROM tb_admin where admin_id='$id'");
    if ($hapus) {
        echo "<script>alert('Data Berhasil Di Hapus');
        window.location='../index.php?page=page/data-admin';
        </script>";
    } else {
        echo "<script>alert('Data Gagal Di Hapus');
        window.location='../index.php?page=page/data-admin';
        </script>";
    }
} elseif (isset($_POST['tambahMember'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $nama = $_POST['nama'];
    $pekerjaan = $_POST['pekerjaan'];
    $alamat = $_POST['alamat'];
    $nohp = $_POST['nohp'];
    $ig = $_POST['ig'];

    $nama_foto = date('Ymd_His') . '_' . $_FILES['foto']['name'];
    $lokasi_foto = $_FILES['foto']['tmp_name'];

    move_uploaded_file($lokasi_foto, '../img/users/member/' . $nama_foto);


    $simpan = $koneksi->query("INSERT INTO tb_member VALUES('','$username','$password','$nama','$pekerjaan','$alamat','$nohp','$ig','$nama_foto')");
    if ($simpan) {
        echo "<script>alert('Data Berhasil Disimpan');
        window.location='../index.php?page=page/data-member';
        </script>";
    } else {
        echo "<script>alert('Data Gagal Disimpan');
        window.location='../index.php?page=page/data-member';
        </script>";
    }
} elseif (isset($_POST['editMember'])) {
    $id = $_POST['memberId'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $nama = $_POST['nama'];
    $pekerjaan = $_POST['pekerjaan'];
    $alamat = $_POST['alamat'];
    $nohp = $_POST['nohp'];
    $ig = $_POST['ig'];

    if (empty($_FILES['foto']['name'])) {
        $edit = $koneksi->query("UPDATE tb_member SET member_username='$username',member_password='$password',member_nama='$nama',member_pekerjaan='$pekerjaan',member_alamat='$alamat',member_nohp='$nohp',member_ig='$ig', member_foto='' where member_id='$id'");
    } else {
        $nama_foto = date('Ymd_His') . '_' . $_FILES['foto']['name'];
        $lokasi_foto = $_FILES['foto']['tmp_name'];

        move_uploaded_file($lokasi_foto, '../img/users/member/' . $nama_foto);

        $edit = $koneksi->query("UPDATE tb_member SET member_username='$username',member_password='$password',member_nama='$nama',member_pekerjaan='$pekerjaan',member_alamat='$alamat',member_nohp='$nohp',member_ig='$ig', member_foto='$nama_foto' where member_id='$id'");
    }


    if ($edit) {
        echo "<script>alert('Data Berhasil Di Edit');
        window.location='../index.php?page=page/data-member';
        </script>";
    } else {
        echo "<script>alert('Data Gagal Di Edit');
        window.location='../index.php?page=page/data-member';
        </script>";
    }
} elseif (isset($_GET['hapusMember'])) {
    $id = $_GET['hapusMember'];
    $queryMember = $koneksi->query("SELECT * FROM tb_member where member_id='$id'");
    $dataMember = $queryMember->fetch_object();

    $gambar = $dataMember->member_foto;
    unlink('../img/users/member/' . $gambar);
    $hapus = $koneksi->query("DELETE FROM tb_member where member_id='$id'");
    if ($hapus) {
        echo "<script>alert('Data Berhasil Di Hapus');
        window.location='../index.php?page=page/data-member';
        </script>";
    } else {
        echo "<script>alert('Data Gagal Di Hapus');
        window.location='../index.php?page=page/data-member';
        </script>";
    }
} elseif (isset($_POST['tambahKategori'])) {
    $namakat = $_POST['namakat'];
    $icon = $_POST['icon'];
    $simpan = $koneksi->query("INSERT INTO tb_kategori VALUES('','$namakat','$icon')");
    if ($simpan) {
        echo "<script>alert('Data Berhasil Disimpan');
        window.location='../index.php?page=page/data-kategori';
        </script>";
    } else {
        echo "<script>alert('Data Gagal Disimpan');
        window.location='../index.php?page=page/data-kategori';
        </script>";
    }
} elseif (isset($_POST['editKategori'])) {
    $id = $_POST['kategoriId'];
    $namakat = $_POST['namakat'];
    $icon = $_POST['icon'];
    $edit = $koneksi->query("UPDATE tb_kategori SET kategori_nama='$namakat', kategori_icon='$icon' where kategori_id='$id'");
    if ($edit) {
        echo "<script>alert('Data Berhasil Di Edit');
        window.location='../index.php?page=page/data-kategori';
        </script>";
    } else {
        echo "<script>alert('Data Gagal Di Edit');
        window.location='../index.php?page=page/data-kategori';
        </script>";
    }
} elseif (isset($_POST['hapusKategori'])) {
    $id = $_POST['kategoriId'];
    $hapus = $koneksi->query("DELETE FROM tb_kategori where kategori_id='$id'");
    if ($hapus) {
        echo "<script>alert('Data Berhasil Di Hapus');
        window.location='../index.php?page=page/data-kategori';
        </script>";
    } else {
        echo "<script>alert('Data Gagal Di Hapus');
        window.location='../index.php?page=page/data-kategori';
        </script>";
    }
} elseif (isset($_POST['tambahBerita'])) {
    session_start();
    $admin_id = $_SESSION['admin']->admin_id;
    $kategori = $_POST['kategori'];
    $judul = $_POST['judul'];
    $isi = $_POST['isi'];

    $nama_foto = date('Ymd_His') . '_' . $_FILES['foto']['name'];
    $lokasi_foto = $_FILES['foto']['tmp_name'];

    move_uploaded_file($lokasi_foto, '../img/berita/' . $nama_foto);


    $simpan = $koneksi->query("INSERT INTO tb_berita VALUES('','$admin_id','$kategori','$judul',now(),'$isi','$nama_foto',0)");
    if ($simpan) {
        echo "<script>alert('Data Berhasil Disimpan');
        window.location='../index.php?page=page/data-berita';
        </script>";
    } else {
        echo "<script>alert('Data Gagal Disimpan');
        window.location='../index.php?page=page/data-berita';
        </script>";
    }
} elseif (isset($_POST['editBerita'])) {
    session_start();
    $admin_id = $_SESSION['admin']->admin_id;
    $id = $_POST['beritaId'];
    $kategori = $_POST['kategori'];
    $judul = $_POST['judul'];
    $isi = $_POST['isi'];
    if (empty($_FILES['foto']['name'])) {
        $edit = $koneksi->query("UPDATE tb_berita SET admin_id='$admin_id', kategori_id='$kategori',berita_judul='$judul',berita_tanggal=now(),berita_isi='$isi', berita_foto='',berita_suka=berita_suka where berita_id='$id'");
    } else {
        $nama_foto = date('Ymd_His') . '_' . $_FILES['foto']['name'];
        $lokasi_foto = $_FILES['foto']['tmp_name'];

        move_uploaded_file($lokasi_foto, '../img/berita/' . $nama_foto);

        $edit = $koneksi->query("UPDATE tb_berita SET admin_id='$admin_id', kategori_id='$kategori',berita_judul='$judul',berita_tanggal=now(),berita_isi='$isi', berita_foto='$nama_foto',berita_suka=berita_suka where berita_id='$id'");
    }


    if ($edit) {
        echo "<script>alert('Data Berhasil Di Edit');
        window.location='../index.php?page=page/data-berita';
        </script>";
    } else {
        echo "<script>alert('Data Gagal Di Edit');
        window.location='../index.php?page=page/data-berita';
        </script>";
    }
} elseif (isset($_GET['hapusBerita'])) {
    $id = $_GET['hapusBerita'];
    $queryBerita = $koneksi->query("SELECT * FROM tb_berita where berita_id='$id'");
    $dataBerita = $queryBerita->fetch_object();

    $gambar = $dataBerita->berita_foto;
    unlink('../img/berita/' . $gambar);
    $hapus = $koneksi->query("DELETE FROM tb_berita where berita_id='$id'");
    if ($hapus) {
        echo "<script>alert('Data Berhasil Di Hapus');
        window.location='../index.php?page=page/data-berita';
        </script>";
    } else {
        echo "<script>alert('Data Gagal Di Hapus');
        window.location='../index.php?page=page/data-berita';
        </script>";
    }
}
