<?php
session_start();

if (empty($_SESSION['admin'])) {
    echo "<script>alert('Silahkan Login Terlebih Dahulu');
    window.location='page/login.php';
    </script>";
}

include 'query/conn.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- META SECTION -->
    <title>Admin - Payakumbuh City</title>
    <?php include 'partials/head.php' ?>
</head>

<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">

        <!-- START PAGE SIDEBAR -->
        <?php include 'partials/side-bar.php' ?>
        <!-- END PAGE SIDEBAR -->

        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <?php include 'partials/top-bar.php' ?>
            <!-- END X-NAVIGATION VERTICAL -->

            <?php include 'content.php' ?>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->

    <!-- MESSAGE BOX-->
    <?php include 'partials/message-box.php' ?>
    <!-- END MESSAGE BOX-->

    <!-- START PRELOADS -->
    <?php include 'partials/audio.php' ?>
    <!-- END PRELOADS -->

    <!-- START SCRIPTS -->
    <?php include 'partials/script.php' ?>
</body>

</html>