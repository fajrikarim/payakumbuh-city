<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <li class="xn-logo text-center">

            <img src="img/logo.png" alt="">

            <a href="" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <?php
                if ($_SESSION['admin']->admin_foto) {
                    $admin_foto = $_SESSION['admin']->admin_foto;
                    echo "<img src='img/users/admin/$admin_foto' alt='Admin Picture' />";
                } else {
                    echo "<img src='img/users/admin/no-image.jpg' alt='Admin Picture' />";
                }
                ?>
            </a>
            <div class="profile">
                <div class="profile-image">
                    <?php
                    if ($_SESSION['admin']->admin_foto) {
                        $admin_foto = $_SESSION['admin']->admin_foto;
                        echo "<img src='img/users/admin/$admin_foto' alt='Admin Picture' />";
                    } else {
                        echo "<img src='img/users/admin/no-image.jpg' alt='Admin Picture' />";
                    }
                    ?>

                </div>
                <div class="profile-data">
                    <div class="profile-data-name"><?php echo $_SESSION['admin']->admin_nama ?></div>
                    <div class="profile-data-title">Admin Payakumbuh City</div>
                </div>
                <div class="profile-controls">
                    <a href="" class="profile-control-left"><span class="fa fa-info"></span></a>
                    <a href="" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                </div>
            </div>
        </li>
        <li class="xn-title">Navigation</li>
        <li>
            <a href="index.php"><span class="fa fa-desktop"></span> <span class="xn-text">Home</span></a>
        </li>

        <li><a href="index.php?page=page/data-admin"><span class="fa fa-users"></span> Admin</a></li>
        <li><a href="index.php?page=page/data-member"><span class="fa fa-users"></span> Member</a></li>
        <li><a href="index.php?page=page/data-kategori"><span class="fa fa-list-ul"></span> Kategori</a></li>
        <li><a href="index.php?page=page/data-berita"><span class="fa fa-keyboard-o"></span> Berita</a></li>
        <li><a href="index.php?page=page/data-cerita-masyarakat"><span class="fa fa-qrcode"></span> Cerita Masyarakat</a></li>


    </ul>
    <!-- END X-NAVIGATION -->
</div>