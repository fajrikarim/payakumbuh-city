<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
            <div class="mb-content">
                <p>Are you sure you want to log out?</p>
                <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <a href="query/query.php?page=logout" class="btn btn-success btn-lg">Yes</a>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ModalTambahAdmin" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-plus"></span> Tambah Admin</h5>
            </div>
            <div class="modal-body">
                <form action="query/query.php" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="">Nama Lengkap</label>
                        <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Anda">
                    </div>
                    <div class="form-group">
                        <label for="">Username</label>
                        <input type="text" name="username" class="form-control" placeholder="Masukkan Username Anda">
                    </div>
                    <div class="form-group">
                        <label for="">Pasword</label>
                        <input type="password" name="password" class="form-control" placeholder="Masukkan Password Anda">
                    </div>
                    <div class="form-group">
                        <label for="">No HP</label>
                        <input type="text" name="nohp" class="form-control" placeholder="Masukkan NoHP Anda">
                    </div>
                    <div class="form-group">
                        <label for="">Username IG</label>
                        <input type="text" name="ig" class="form-control" placeholder="Masukkan Username Instagram">
                    </div>
                    <div class="form-group">
                        <label for="">Foto</label>
                        <input type="file" name="foto" class="form-control" placeholder="Masukkan Foto Anda">
                    </div>
            </div>
            <div class="modal-footer">
                <button name="tambahAdmin" class="btn btn-primary text-white">Simpan</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ModalTambahMember" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-plus"></span> Tambah Member</h5>
            </div>
            <div class="modal-body">
                <form action="query/query.php" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="">Nama Lengkap</label>
                        <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Anda">
                    </div>
                    <div class="form-group">
                        <label for="">Username</label>
                        <input type="text" name="username" class="form-control" placeholder="Masukkan Username Anda">
                    </div>
                    <div class="form-group">
                        <label for="">Pasword</label>
                        <input type="password" name="password" class="form-control" placeholder="Masukkan Password Anda">
                    </div>
                    <div class="form-group">
                        <label for="">Pekerjaan</label>
                        <input type="text" name="pekerjaan" class="form-control" placeholder="Masukkan Pekerjaan Anda">
                    </div>
                    <div class="form-group">
                        <label for="">Alamat</label>
                        <textarea name="alamat" cols="30" rows="3" class="form-control" placeholder="Masukkan Alamat Anda"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">No HP</label>
                        <input type="text" name="nohp" class="form-control" placeholder="Masukkan NoHP Anda">
                    </div>
                    <div class="form-group">
                        <label for="">Username Instagram</label>
                        <input type="text" name="ig" class="form-control" placeholder="Masukkan Username Instagram">
                    </div>
                    <div class="form-group">
                        <label for="">Foto</label>
                        <input type="file" name="foto" class="form-control" placeholder="Masukkan Foto Anda">
                    </div>
            </div>
            <div class="modal-footer">
                <button name="tambahMember" class="btn btn-primary text-white">Simpan</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ModalTambahKategori" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-plus"></span> Tambah Kategori</h5>
            </div>
            <div class="modal-body">
                <form action="query/query.php" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="">Nama Kategori</label>
                        <input type="text" name="namakat" class="form-control" placeholder="Masukkan Nama Kategori">
                    </div>
                    <div class="form-group">
                        <label for="">Icon</label>
                        <input type="text" name="icon" class="form-control" placeholder="Masukkan Icon Kategori">
                    </div>
            </div>
            <div class="modal-footer">
                <button name="tambahKategori" class="btn btn-primary text-white">Simpan</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ModalTambahBerita" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-plus"></span> Tambah Berita</h5>
            </div>
            <div class="modal-body">
                <form action="query/query.php" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="">Kategori</label>
                        <select name="kategori" class="form-control">
                            <option value="0">--- Pilih Kategori ---</option>
                            <?php
                            $queryKat = $koneksi->query("SELECT * FROM tb_kategori");
                            while ($dataKat = $queryKat->fetch_object()) {
                                echo "<option value='$dataKat->kategori_id'>$dataKat->kategori_nama</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Judul Berita</label>
                        <input type="text" name="judul" class="form-control" placeholder="Masukkan Judul Berita">
                    </div>
                    <div class="form-group">
                        <label for="">Isi Berita</label>
                        <textarea name="isi" class="summernote" cols="30" rows="13"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Foto Berita</label>
                        <input type="file" name="foto" class="form-control" placeholder="Masukkan Foto">
                    </div>
            </div>
            <div class="modal-footer">
                <button name="tambahBerita" class="btn btn-primary text-white">Simpan</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                </form>
            </div>
        </div>
    </div>
</div>