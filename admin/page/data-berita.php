<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="index.php">Home</a></li>
    <li class="active">Berita</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalTambahBerita">
                            <span class="fa fa-plus"></span> Tambah Berita
                        </button>
                    </h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="50px">No</th>
                                <th>Kategori</th>
                                <th>Admin</th>
                                <th>Judul Berita</th>
                                <th>Tanggal</th>
                                <th>Disukai</th>
                                <th width="190px" class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $queryBerita = $koneksi->query("SELECT * FROM tb_berita JOIN tb_admin ON tb_berita.admin_id=tb_admin.admin_id JOIN tb_kategori ON tb_berita.kategori_id=tb_kategori.kategori_id");
                            while ($dataBerita = $queryBerita->fetch_object()) {

                            ?>
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo $dataBerita->kategori_nama ?></td>
                                    <td><?php echo $dataBerita->admin_nama ?></td>
                                    <td><?php echo $dataBerita->berita_judul ?></td>
                                    <td><?php echo $dataBerita->berita_tanggal ?></td>
                                    <td><?php echo $dataBerita->berita_suka ?></td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalDetailBerita<?php echo $dataBerita->berita_id ?>">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                        <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#ModalEditBerita<?php echo $dataBerita->berita_id ?>">
                                            <span class="fa fa-edit"></span>
                                        </button>
                                        <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#ModalHapusBerita<?php echo $dataBerita->berita_id ?>">
                                            <span class="fa fa-trash-o"></span>
                                        </button>
                                    </td>
                                </tr>
                                <div class="modal fade" id="ModalDetailBerita<?php echo $dataBerita->berita_id ?>" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-eye"></span> Detail Data Berita</h5>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-5 text-center">
                                                        <div class="panel">
                                                            <div class="panel-heading text-center">
                                                                <h3 class="">Foto Profile</h3>
                                                            </div>
                                                            <div class="panel-body">
                                                                <?php
                                                                if ($dataBerita->berita_foto) {
                                                                    $berita_foto = $dataBerita->berita_foto;
                                                                    echo "<img src='img/berita/$berita_foto' width='250px' height='150px' alt='Berita Picture' />";
                                                                } else {
                                                                    echo "<img src='img/berita/2.jpg' width='250px' height='150px' alt='Berita Picture' />";
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <div class="panel">
                                                            <div class="panel-heading text-center">
                                                                <h3 class="">Informasi</h3>
                                                            </div>
                                                            <div class="panel-body">
                                                                <p>
                                                                    Penulis :<br>- <?php echo $dataBerita->admin_nama ?>
                                                                </p>
                                                                <p>
                                                                    Tanggal :<br>- <?php echo $dataBerita->berita_tanggal ?>
                                                                </p>
                                                                <p>
                                                                    Kategori :<br>- <?php echo $dataBerita->kategori_nama ?>
                                                                </p>
                                                                <p>
                                                                    Disukai :<br>- <?php echo $dataBerita->berita_suka ?>
                                                                </p>
                                                                <p>
                                                                    Judul :<br>- <?php echo $dataBerita->berita_judul ?>
                                                                </p>
                                                                <p>
                                                                    Isi :<br>- <?php echo $dataBerita->berita_isi ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="ModalEditBerita<?php echo $dataBerita->berita_id ?>" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-edit"></span> Edit Data Berita</h5>
                                            </div>
                                            <div class="modal-body">
                                                <form action="query/query.php" method="POST" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label for="">Kategori</label>
                                                        <select name="kategori" class="form-control">
                                                            <option value="<?php echo $dataBerita->berita_id ?>"><?php echo $dataBerita->kategori_nama ?></option>
                                                            <option value="0"></option>
                                                            <option value="0">--- Ganti Kategori ---</option>
                                                            <?php
                                                            $queryKat = $koneksi->query("SELECT * FROM tb_kategori");
                                                            while ($dataKat = $queryKat->fetch_object()) {
                                                                echo "<option value='$dataKat->kategori_id'>$dataKat->kategori_nama</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Judul Berita</label>
                                                        <input type="hidden" name="beritaId" class="form-control" value="<?php echo $dataBerita->berita_id ?>">
                                                        <input type="text" name="judul" value="<?php echo $dataBerita->berita_judul ?>" class="form-control" placeholder="Masukkan Judul Berita">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Isi Berita</label>
                                                        <textarea name="isi" class="form-control" cols="30" rows="13"><?php echo $dataBerita->berita_isi ?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Foto Berita</label>
                                                        <input type="file" name="foto" class="form-control" placeholder="Masukkan Foto">
                                                    </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button name="editBerita" class="btn btn-primary text-white">Simpan</button>
                                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="ModalHapusBerita<?php echo $dataBerita->berita_id ?>" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-user-plus"></span> Konfirmasi</h5>
                                            </div>
                                            <div class="modal-body">
                                                <h3>Apakah Anda Yakin Ingin Hapus Berita '<?php echo $dataBerita->berita_judul ?>' ?</h3>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="query/query.php?hapusBerita=<?php echo $dataBerita->berita_id ?>" class="btn btn-danger">Ya</a>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>

</div>
<!-- END PAGE CONTENT WRAPPER -->