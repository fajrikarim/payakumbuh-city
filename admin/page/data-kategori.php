<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="index.php">Home</a></li>
    <li class="active">Kategori</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalTambahKategori">
                            <span class="fa fa-plus"></span> Tambah Kategori
                        </button>
                    </h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="50px">No</th>
                                <th>Nama Kategori</th>
                                <th>Icon Kategori</th>
                                <th width="190px" class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $queryKategori = $koneksi->query("SELECT * FROM tb_kategori");
                            while ($dataKategori = $queryKategori->fetch_object()) {

                            ?>
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo $dataKategori->kategori_nama ?></td>
                                    <td>[ <span class="<?php echo $dataKategori->kategori_icon ?>"></span> ] <?php echo $dataKategori->kategori_icon ?></td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalDetailKategori<?php echo $dataKategori->kategori_id ?>">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                        <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#ModalEditKategori<?php echo $dataKategori->kategori_id ?>">
                                            <span class="fa fa-edit"></span>
                                        </button>
                                        <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#ModalHapusKategori<?php echo $dataKategori->kategori_id ?>">
                                            <span class="fa fa-trash-o"></span>
                                        </button>
                                    </td>
                                </tr>
                                <div class="modal fade" id="ModalDetailKategori<?php echo $dataKategori->kategori_id ?>" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-eye"></span> Detail Data Kategori</h5>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    Nama Kategori :<br>- <?php echo $dataKategori->kategori_nama ?>
                                                </p>
                                                <p>
                                                    Icon Kategori :<br>- [ <span class="<?php echo $dataKategori->kategori_icon ?>"></span> ] <?php echo $dataKategori->kategori_icon ?>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="ModalEditKategori<?php echo $dataKategori->kategori_id ?>" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-edit"></span> Edit Data Kategori</h5>
                                            </div>
                                            <div class="modal-body">
                                                <form action="query/query.php" method="POST" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label for="">Nama Kategori</label>
                                                        <input type="hidden" name="kategoriId" class="form-control" value="<?php echo $dataKategori->kategori_id ?>" placeholder="Masukkan Nama Anda">
                                                        <input type="text" name="namakat" class="form-control" value="<?php echo $dataKategori->kategori_nama ?>" placeholder="Masukkan Nama Kategori">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Icon Kategori</label>
                                                        <input type="text" name="icon" class="form-control" value="<?php echo $dataKategori->kategori_icon ?>" placeholder="Masukkan Icon Kategori">
                                                    </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button name="editKategori" class="btn btn-primary text-white">Simpan</button>
                                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="ModalHapusKategori<?php echo $dataKategori->kategori_id ?>" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-user-plus"></span> Konfirmasi</h5>
                                            </div>
                                            <div class="modal-body">
                                                <h3>Apakah Anda Yakin Ingin Hapus Data Kategori '<?php echo $dataKategori->kategori_nama ?>' ?</h3>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="query/query.php?hapusKategori=<?php echo $dataKategori->kategori_id ?>" class="btn btn-danger">Ya</a>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>

</div>
<!-- END PAGE CONTENT WRAPPER -->