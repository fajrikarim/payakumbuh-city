<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="index.php">Home</a></li>
    <li class="active">Member</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalTambahMember">
                            <span class="fa fa-plus"></span> Tambah Member
                        </button>
                    </h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="50px">No</th>
                                <th width="120px">Foto Member</th>
                                <th>Nama Member</th>
                                <th>Pekerjaan</th>
                                <th>Username</th>
                                <th>No HP</th>
                                <th width="190px" class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $queryMember = $koneksi->query("SELECT * FROM tb_member");
                            while ($dataMember = $queryMember->fetch_object()) {

                            ?>
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td>
                                        <?php
                                        if ($dataMember->member_foto) {
                                            $member_foto = $dataMember->member_foto;
                                            echo "<img src='img/users/member/$member_foto' width='50px' height='50px' alt='Member Picture' />";
                                        } else {
                                            echo "<img src='img/users/member/no-image.jpg' width='50px' height='50px' alt='Member Picture' />";
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo $dataMember->member_nama ?></td>
                                    <td><?php echo $dataMember->member_pekerjaan ?></td>
                                    <td><?php echo $dataMember->member_username ?></td>
                                    <td><?php echo $dataMember->member_nohp ?></td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalDetailMember<?php echo $dataMember->member_id ?>">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                        <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#ModalEditMember<?php echo $dataMember->member_id ?>">
                                            <span class="fa fa-edit"></span>
                                        </button>
                                        <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#ModalHapusMember<?php echo $dataMember->member_id ?>">
                                            <span class="fa fa-trash-o"></span>
                                        </button>
                                    </td>
                                </tr>
                                <div class="modal fade" id="ModalDetailMember<?php echo $dataMember->member_id ?>" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-eye"></span> Detail Data Member</h5>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-5 text-center">
                                                        <div class="panel">
                                                            <div class="panel-heading text-center">
                                                                <h3 class="">Foto Profile</h3>
                                                            </div>
                                                            <div class="panel-body">
                                                                <?php
                                                                if ($dataMember->member_foto) {
                                                                    $member_foto = $dataMember->member_foto;
                                                                    echo "<img src='img/users/member/$member_foto' width='150px' height='150px' alt='Member Picture' />";
                                                                } else {
                                                                    echo "<img src='img/users/member/no-image.jpg' width='150px' height='150px' alt='Member Picture' />";
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <div class="panel">
                                                            <div class="panel-heading text-center">
                                                                <h3 class="">Informasi</h3>
                                                            </div>
                                                            <div class="panel-body">
                                                                <p>
                                                                    Nama :<br>- <?php echo $dataMember->member_nama ?>
                                                                </p>
                                                                <p>
                                                                    Instagram :<br>- <a href="https://instagram.com/<?php echo $dataMember->member_ig ?>"><?php echo $dataMember->member_ig ?></a>
                                                                </p>
                                                                <p>
                                                                    Username :<br>- <?php echo $dataMember->member_username ?>
                                                                </p>
                                                                <p>
                                                                    Password :<br>- <?php echo $dataMember->member_password ?>
                                                                </p>
                                                                <p>
                                                                    Pekerjaan :<br>- <?php echo $dataMember->member_pekerjaan ?>
                                                                </p>
                                                                <p>
                                                                    Alamat :<br>- <?php echo $dataMember->member_alamat ?>
                                                                </p>
                                                                <p>
                                                                    NoHp :<br>- <?php echo $dataMember->member_nohp ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="ModalEditMember<?php echo $dataMember->member_id ?>" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-edit"></span> Edit Data Member</h5>
                                            </div>
                                            <div class="modal-body">
                                                <form action="query/query.php" method="POST" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label for="">Nama Lengkap</label>
                                                        <input type="hidden" name="memberId" class="form-control" value="<?php echo $dataMember->member_id ?>" placeholder="Masukkan Nama Anda">
                                                        <input type="text" name="nama" class="form-control" value="<?php echo $dataMember->member_nama ?>" placeholder="Masukkan Nama Anda">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Username</label>
                                                        <input type="text" name="username" class="form-control" value="<?php echo $dataMember->member_username ?>" placeholder="Masukkan Username Anda">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Pasword</label>
                                                        <input type="password" name="password" class="form-control" value="<?php echo $dataMember->member_password ?>" placeholder="Masukkan Password Anda">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Pekerjaan</label>
                                                        <input type="text" name="pekerjaan" class="form-control" value="<?php echo $dataMember->member_pekerjaan ?>" placeholder="Masukkan Pekerjaan Anda">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Alamat</label>
                                                        <textarea name="alamat" cols="30" rows="3" class="form-control" placeholder="Masukkan Alamat Anda"><?php echo $dataMember->member_alamat ?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">No HP</label>
                                                        <input type="text" name="nohp" class="form-control" value="<?php echo $dataMember->member_nohp ?>" placeholder="Masukkan NoHP Anda">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Username IG</label>
                                                        <input type="text" name="ig" class="form-control" value="<?php echo $dataMember->member_ig ?>" placeholder="Masukkan Username Istagram">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Foto</label>
                                                        <input type="file" name="foto" class="form-control" placeholder="Masukkan Foto Anda">
                                                    </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button name="editMember" class="btn btn-primary text-white">Simpan</button>
                                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="ModalHapusMember<?php echo $dataMember->member_id ?>" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-user-plus"></span> Konfirmasi</h5>
                                            </div>
                                            <div class="modal-body">
                                                <h3>Apakah Anda Yakin Ingin Hapus Data Akun '<?php echo $dataMember->member_nama ?>' ?</h3>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="query/query.php?hapusMember=<?php echo $dataMember->member_id ?>" class="btn btn-danger">Ya</a>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>

</div>
<!-- END PAGE CONTENT WRAPPER -->