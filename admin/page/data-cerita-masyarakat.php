<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="index.php">Home</a></li>
    <li class="active">Cerita Masyarakat</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Cerita Masyarakat
                    </h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="50px">No</th>
                                <th>Member</th>
                                <th>Judul Cerita</th>
                                <th>Tanggal</th>
                                <th>Disukai</th>
                                <th>Status</th>
                                <th width="190px" class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $queryCM = $koneksi->query("SELECT * FROM tb_cerita_masyarakat JOIN tb_member ON tb_cerita_masyarakat.member_id=tb_member.member_id");
                            while ($dataCM = $queryCM->fetch_object()) {

                            ?>
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo $dataCM->member_nama ?></td>
                                    <td><?php echo $dataCM->cm_judul ?></td>
                                    <td><?php echo $dataCM->cm_tanggal ?></td>
                                    <td><?php echo $dataCM->cm_suka ?></td>
                                    <td><?php echo $dataCM->cm_status ?></td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalDetailCM<?php echo $dataCM->cm_id ?>">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                        <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#ModalHapusCM<?php echo $dataCM->cm_id ?>">
                                            <span class="fa fa-trash-o"></span>
                                        </button>
                                    </td>
                                </tr>
                                <div class="modal fade" id="ModalDetailCM<?php echo $dataCM->cm_id ?>" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-eye"></span> Detail Cerita Masyarakat</h5>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-5 text-center">
                                                        <div class="panel">
                                                            <div class="panel-heading text-center">
                                                                <h3 class="">Foto Profile</h3>
                                                            </div>
                                                            <div class="panel-body">
                                                                <?php
                                                                if ($dataCM->cm_foto) {
                                                                    $cm_foto = $dataCM->cm_foto;
                                                                    echo "<img src='img/cm/$cm_foto' width='250px' height='150px' alt='Cerita Masyarakat Picture' />";
                                                                } else {
                                                                    echo "<img src='img/cm/2.jpg' width='250px' height='150px' alt='Cerita Masyarakat Picture' />";
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <div class="panel">
                                                            <div class="panel-heading text-center">
                                                                <h3 class="">Informasi</h3>
                                                            </div>
                                                            <div class="panel-body">
                                                                <p>
                                                                    Status :<br>- <?php echo $dataCM->cm_status ?>
                                                                </p>
                                                                <p>
                                                                    Penulis :<br>- <?php echo $dataCM->member_nama ?>
                                                                </p>
                                                                <p>
                                                                    Tanggal :<br>- <?php echo $dataCM->cm_tanggal ?>
                                                                </p>
                                                                <p>
                                                                    Disukai :<br>- <?php echo $dataCM->cm_suka ?>
                                                                </p>
                                                                <p>
                                                                    Judul :<br>- <?php echo $dataCM->cm_judul ?>
                                                                </p>
                                                                <p>
                                                                    Isi :<br>- <?php echo $dataCM->cm_isi ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="ModalHapusCM<?php echo $dataCM->CM_id ?>" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-user-plus"></span> Konfirmasi</h5>
                                            </div>
                                            <div class="modal-body">
                                                <h3>Apakah Anda Yakin Ingin Hapus Cerita '<?php echo $dataCM->cm_judul ?>' ?</h3>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="query/query.php?hapusCM=<?php echo $dataCM->cm_id ?>" class="btn btn-danger">Ya</a>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>

</div>
<!-- END PAGE CONTENT WRAPPER -->