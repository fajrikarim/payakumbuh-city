<?php
$queryBeritaUtama = $koneksi->query("SELECT * FROM tb_berita JOIN tb_kategori ON tb_berita.kategori_id=tb_kategori.kategori_id JOIN tb_admin ON tb_berita.admin_id=tb_admin.admin_id ORDER BY tb_berita.berita_suka DESC LIMIT 1");
while ($dataBeritaUtama = $queryBeritaUtama->fetch_object()) {

?>
    <div class="col-12 col-md-6 col-lg-8">
        <div class="row">
            <!-- Single Featured Post -->
            <div class="col-12 col-lg-12">
                <div class="single-blog-post featured-post">
                    <div class="post-thumb">
                        <a href="index.php?page=page/detail&id=<?php echo $dataBeritaUtama->berita_id ?>"><img src="admin/img/berita/<?php echo $dataBeritaUtama->berita_foto ?>" alt=""></a>
                    </div>
                    <div class="post-data">
                        <a href="index.php?page=page/detail&id=<?php echo $dataBeritaUtama->berita_id ?>" class="post-catagory"><span class="<?php echo $dataBeritaUtama->kategori_icon ?>"></span>
                            <?php echo $dataBeritaUtama->kategori_nama ?></a>
                        <a href="index.php?page=page/detail&id=<?php echo $dataBeritaUtama->berita_id ?>" class="post-title">
                            <h6><?php echo $dataBeritaUtama->berita_judul ?>
                            </h6>
                        </a>
                        <div class="post-meta">
                            <p class="post-author">Penulis : <a href="https://www.instagram.com/<?php echo $dataBeritaUtama->admin_ig ?>" target="_blank"><?php echo $dataBeritaUtama->admin_nama ?></a></p>
                            <p class="post-excerp text-justify">
                                <?php echo substr($dataBeritaUtama->berita_isi, 0, 312) ?>
                            </p>
                            <!-- Post Like & Post Comment -->
                            <div class="d-flex align-items-center">
                                <a href="#" class="post-like"><img src="img/core-img/like.png" alt="">
                                    <span><?php echo $dataBeritaUtama->berita_suka ?></span></a>
                                <a href="#" class="post-comment"><img src="img/core-img/chat.png" alt="">
                                    <span>10</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>