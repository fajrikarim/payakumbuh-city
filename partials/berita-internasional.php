<div class="col-12 col-md-5 col-lg-3">
    <div class="section-heading">
        <h6>Berita Internasional</h6>
    </div>

    <?php
    $queryBeritaInternasional = $koneksi->query("SELECT * FROM tb_berita JOIN tb_kategori ON tb_berita.kategori_id=tb_kategori.kategori_id JOIN tb_admin ON tb_berita.admin_id=tb_admin.admin_id where tb_kategori.kategori_nama='Internasional' LIMIT 4");
    while ($dataBeritaInternasional = $queryBeritaInternasional->fetch_object()) {

    ?>
        <!-- Single Post -->
        <div class="single-blog-post style-2">
            <div class="post-thumb">
                <a href="index.php?page=page/detail&id=<?php echo $dataBeritaInternasional->berita_id ?>"><img src="admin/img/berita/<?php echo $dataBeritaInternasional->berita_foto ?>" style="height:110px; width:270px" alt=""></a>
            </div>
            <div class="post-data">
                <a href="berita1.html" class="post-title">
                    <h6><?php echo $dataBeritaInternasional->berita_judul ?></h6>
                </a>
                <div class="post-meta">
                    <div class="post-date"><a href="berita1.html"><?php echo date('l , d F Y', strtotime($dataBeritaInternasional->berita_tanggal)) ?></a></div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>