<header class="header-area">

    <!-- Top Header Area -->
    <div class="top-header-area bg-info">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="top-header-content d-flex align-items-center justify-content-between bg-info">
                        <!-- Logo -->
                        <div class="logo">
                            <a href="index.html"><img src="img/core-img/logo.png" alt=""></a>
                        </div>

                        <!-- Login Search Area -->
                        <div class="login-search-area d-flex align-items-center">
                            <!-- Login -->
                            <div class="login d-flex">
                                <?php
                                if (empty($_SESSION['member'])) {

                                ?>
                                    <a href="#" data-toggle="modal" data-target="#ModalLogin"> <span class="fa fa-user"></span> Masuk</a>
                                    <a href="#" data-toggle="modal" data-target="#ModalDaftar"> <span class="fa fa-user-plus"></span> Daftar</a>
                                <?php
                                } else {

                                ?>
                                    <a href="#" data-toggle="modal" data-target="#ModalProfile"> <span class="fa fa-user"></span> <?php echo $_SESSION['member']->member_nama; ?></a>
                                    <a href="#" data-toggle="modal" data-target="#ModalLogout"> <span class="fa fa-sign-out"></span> Logout</a>
                                <?php
                                }
                                ?>
                            </div>
                            <!-- Search Form -->
                            <div class="search-form">
                                <form action="#" method="post">
                                    <input type="search" name="search" class="form-control" placeholder="Search">
                                    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Navbar Area -->
    <div class="newspaper-main-menu" id="stickyMenu">
        <div class="classy-nav-container breakpoint-off">
            <div class="container">
                <!-- Menu -->
                <nav class="classy-navbar justify-content-between" id="newspaperNav">

                    <!-- Logo -->
                    <div class="logo">
                        <a href="index.html"><img src="img/core-img/logo.png" alt=""></a>
                    </div>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- close btn -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul>
                                <li><a href="index.php"><span class="fa fa-home"></span> Home
                                    </a>
                                </li>
                                <?php
                                $queryKategori = $koneksi->query("SELECT * FROM tb_kategori LIMIT 8");
                                while ($dataKategori = $queryKategori->fetch_object()) {

                                ?>
                                    <li><a href="index.php?page=page/kategori&id=<?php echo $dataKategori->kategori_iid ?>"><span class="<?php echo $dataKategori->kategori_icon ?>"></span> <?php echo $dataKategori->kategori_nama ?></a></li>

                                <?php } ?>
                                <li><a href="contact.html"><span class="fa fa-comment"></span> Kontak</a></li>
                                <li><a href="about.html"><span class="fa fa-address-card"></span> Tentang</a>
                                </li>
                            </ul>
                        </div>
                        <!-- Nav End -->
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>