<footer class="footer-area">

    <!-- Main Footer Area -->
    <div class="main-footer-area">
        <div class="container">
            <div class="row">

                <!-- Footer Widget Area -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="footer-widget-area mt-80">
                        <!-- Footer Logo -->
                        <div class="footer-logo">
                            <a href="index.html"><img src="img/core-img/logo.png" alt=""></a>
                        </div>
                        <!-- List -->
                        <ul class="list">
                            <li><a href="mailto:fajrikarim@gmail.com">fajrikarim@gmail.com</a></li>
                            <li><a href="tel:+6282382823732">+6282382823732</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Footer Widget Area -->
                <div class="col-12 col-sm-6 col-lg-2">
                    <div class="footer-widget-area mt-80">
                        <!-- Title -->
                        <h4 class="widget-title">Kategori</h4>
                        <!-- List -->
                        <ul class="list">

                            <?php
                            $queryKategori = $koneksi->query("SELECT * FROM tb_kategori");
                            while ($dataKategori = $queryKategori->fetch_object()) {

                            ?>
                                <li><a href="index.php?page=kategori&id=<?php echo $dataKategori->kategori_nama ?>"><?php echo $dataKategori->kategori_nama ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>

                <!-- Footer Widget Area -->
                <div class="col-12 col-sm-4 col-lg-2">
                    <div class="footer-widget-area mt-80">
                        <!-- Title -->
                        <h4 class="widget-title">Games</h4>
                        <!-- List -->
                        <ul class="list">
                            <li><a href="kat_viral.html">PUBG</a></li>
                            <li><a href="kat_viral.html">Mobile Legend</a></li>
                            <li><a href="kat_viral.html">Perfect World</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Footer Widget Area -->
                <div class="col-12 col-sm-4 col-lg-2">
                    <div class="footer-widget-area mt-80">
                        <!-- Title -->
                        <h4 class="widget-title">Olahraga</h4>
                        <!-- List -->
                        <ul class="list">
                            <li><a href="kat_viral.html">Bola</a></li>
                            <li><a href="kat_viral.html">Basket</a></li>
                            <li><a href="kat_viral.html">Voli</a></li>
                            <li><a href="kat_viral.html">Renang</a></li>
                            <li><a href="kat_viral.html">Panahan</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Footer Widget Area -->
                <div class="col-12 col-sm-4 col-lg-2">
                    <div class="footer-widget-area mt-80">
                        <!-- Title -->
                        <h4 class="widget-title">Makanan</h4>
                        <!-- List -->
                        <ul class="list">
                            <li><a href="kat_viral.html">Rendang</a></li>
                            <li><a href="kat_viral.html">Galamai</a></li>
                            <li><a href="kat_viral.html">Bareh Randang</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bottom Footer Area -->
    <div class="bottom-footer-area">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <!-- Copywrite -->
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                        </script> Fajri Karim</p>
                </div>
            </div>
        </div>
    </div>
</footer>