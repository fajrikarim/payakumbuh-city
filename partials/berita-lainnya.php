<div class="col-12 col-lg-4">
    <!-- Breaking News Widget -->
    <div class="breaking-news-area d-flex align-items-center">
        <div class="news-title bg-info">
            <p>Berita Terbaru <span class="fa fa-fire"></span> </p>
        </div>
        <div id="breakingNewsTicker" class="ticker">
            <ul>
                <li><a href="berita1.html">Wakanda Minang</a></li>
                <li><a href="berita1.html">New Zealand Minang</a></li>
                <li><a href="berita1.html">Harau Valley!</a></li>
            </ul>
        </div>
    </div>

    <!-- Breaking News Widget -->
    <div class="breaking-news-area d-flex align-items-center mt-15">
        <div class="news-title title2">
            <p>Berita Viral <span class="fa fa-ravelry"></span> </p>
        </div>
        <div id="internationalTicker" class="ticker">
            <ul>
                <li><a href="berita1.html">Wakanda Minang</a></li>
                <li><a href="berita1.html">New Zealand Minang</a></li>
                <li><a href="berita1.html">Harau Valley!</a></li>
            </ul>
        </div>
    </div>
    <!-- Single Featured Post -->
    <?php
    $queryBeritaLain = $koneksi->query("SELECT * FROM tb_berita JOIN tb_kategori ON tb_berita.kategori_id=tb_kategori.kategori_id JOIN tb_admin ON tb_berita.admin_id=tb_admin.admin_id LIMIT 6");
    while ($dataBeritaLain = $queryBeritaLain->fetch_object()) {

    ?>
        <div class="single-blog-post small-featured-post d-flex">
            <div class="post-thumb">
                <a href="index.php?page=page/detail&id=<?php echo $dataBeritaLain->berita_id ?>"><img src="admin/img/berita/<?php echo $dataBeritaLain->berita_foto ?>" style="height: 80px" alt=""></a>
            </div>
            <div class="post-data">
                <a href="berita1.html" class="post-catagory">
                    <?php echo $dataBeritaLain->kategori_nama ?>
                </a>
                <div class="post-meta">
                    <a href="berita1.html" class="post-title">
                        <h6><?php echo $dataBeritaLain->berita_judul ?></h6>
                    </a>
                    <p class="post-date"><?php echo date('l , d F Y', strtotime($dataBeritaLain->berita_tanggal)) ?></p>
                </div>
            </div>
        </div>
    <?php } ?>
</div>