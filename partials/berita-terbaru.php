<div class="col-12 col-lg-8">
    <div class="section-heading">
        <h6>Berita Terbaru Lainnya</h6>
    </div>

    <div class="row">
        <?php
        $queryBeritaTerbaru = $koneksi->query("SELECT * FROM tb_berita JOIN tb_kategori ON tb_berita.kategori_id=tb_kategori.kategori_id JOIN tb_admin ON tb_berita.admin_id=tb_admin.admin_id LIMIT 4");
        while ($dataBeritaTerbaru = $queryBeritaTerbaru->fetch_object()) {

        ?>
            <!-- Single Post -->
            <div class="col-12 col-md-6">
                <div class="single-blog-post style-3">
                    <div class="post-thumb">
                        <a href="index.php?page=page/detail&id=<?php echo $dataBeritaTerbaru->berita_id ?>"><img src="admin/img/berita/<?php echo $dataBeritaTerbaru->berita_foto ?>" style="height: 250px" alt=""></a>
                    </div>
                    <div class="post-data">
                        <a href="index.php?page=page/detail&id=<?php echo $dataBeritaTerbaru->berita_id ?>" class="post-catagory"><?php echo $dataBeritaTerbaru->kategori_nama ?></a>
                        <a href="index.php?page=page/detail&id=<?php echo $dataBeritaTerbaru->berita_id ?>" class="post-title">
                            <h6><?php echo $dataBeritaTerbaru->berita_judul ?></h6>
                        </a>
                        <div class="post-meta d-flex align-items-center">
                            <a class="post-like"><img src="img/core-img/like.png" alt=""> <span><?php echo $dataBeritaTerbaru->berita_suka ?></span></a>
                            <a class="post-comment"><img src="img/core-img/chat.png" alt=""> <span>10</span></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>