<div class="col-12 col-lg-4">
    <div class="section-heading">
        <h6>Terpupuler</h6>
    </div>
    <!-- Popular News Widget -->
    <div class="popular-news-widget mb-30">
        <h3>4 Berita Terpopuler</h3>

        <?php
        $no = 1;
        $queryBeritaPupuler = $koneksi->query("SELECT * FROM tb_berita JOIN tb_kategori ON tb_berita.kategori_id=tb_kategori.kategori_id JOIN tb_admin ON tb_berita.admin_id=tb_admin.admin_id ORDER BY tb_berita.berita_suka DESC LIMIT 4");
        while ($dataBeritaPupuler = $queryBeritaPupuler->fetch_object()) {

        ?>
            <!-- Single Popular Blog -->
            <div class="single-popular-post">
                <a href="index.php?page=page/detail&id=<?php echo $dataBeritaPupuler->berita_id ?>">
                    <h6><span><?php echo $no++ ?>.</span>
                        <?php echo $dataBeritaPupuler->berita_judul ?>
                    </h6>
                </a>
                <p><?php echo date('l , d F Y', strtotime($dataBeritaPupuler->berita_tanggal)) ?></p>
            </div>
        <?php } ?>
    </div>