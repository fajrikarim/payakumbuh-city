<div class="col-12 col-md-7 col-lg-9">
    <div class="section-heading">
        <h6>Cerita Masyarakat</h6>
    </div>

    <div class="row">

        <!-- Single Post -->
        <div class="col-12 col-lg-4">
            <div class="single-blog-post">
                <div class="post-thumb">
                    <a href="berita1.html"><img src="img/bg-img/1.jpg" alt=""></a>
                </div>
                <div class="post-data">
                    <a href="berita1.html" class="post-title">
                        <h6>Buku Adalah Dunia yang dituliskan.</h6>
                    </a>
                    <div class="post-meta">
                        <div class="post-date"><a href="berita1.html">February 11, 2018</a></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Single Post -->
        <div class="col-12 col-lg-4">
            <div class="single-blog-post">
                <div class="post-thumb">
                    <a href="berita1.html"><img src="img/bg-img/2.jpg" alt=""></a>
                </div>
                <div class="post-data">
                    <a href="berita1.html" class="post-title">
                        <h6>Rumah Idaman.</h6>
                    </a>
                    <div class="post-meta">
                        <div class="post-date"><a href="berita1.html">February 11, 2018</a></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Single Post -->
        <div class="col-12 col-lg-4">
            <div class="single-blog-post">
                <div class="post-thumb">
                    <a href="berita1.html"><img src="img/bg-img/3.jpg" alt=""></a>
                </div>
                <div class="post-data">
                    <a href="berita1.html" class="post-title">
                        <h6>Lonjakan keuangan keluarga.</h6>
                    </a>
                    <div class="post-meta">
                        <div class="post-date"><a href="berita1.html">February 11, 2018</a></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Single Post -->
        <div class="col-12 col-lg-4">
            <div class="single-blog-post">
                <div class="post-thumb">
                    <a href="berita1.html"><img src="img/bg-img/4.jpg" alt=""></a>
                </div>
                <div class="post-data">
                    <a href="berita1.html" class="post-title">
                        <h6>Tips Belajar Cepat HTML.</h6>
                    </a>
                    <div class="post-meta">
                        <div class="post-date"><a href="berita1.html">February 11, 2018</a></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Single Post -->
        <div class="col-12 col-lg-4">
            <div class="single-blog-post">
                <div class="post-thumb">
                    <a href="berita1.html"><img src="img/bg-img/5.jpg" alt=""></a>
                </div>
                <div class="post-data">
                    <a href="berita1.html" class="post-title">
                        <h6>Berkerja dalam tim.</h6>
                    </a>
                    <div class="post-meta">
                        <div class="post-date"><a href="berita1.html">February 11, 2018</a></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Single Post -->

    </div>
</div>