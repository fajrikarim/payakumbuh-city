<div class="modal fade" id="ModalLogin" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-user"></span> Masuk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <form action="query/query.php" method="post">
                    <div class="row">
                        <div class="col-3 text-right">
                            <label for="" class="mt-2">Username </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" type="text" name="username" placeholder="Masukkan Username Anda !">
                        </div>
                        <div class="col-3 text-right">
                            <br>
                            <label for="" class="mt-2">Password </label>
                        </div>
                        <div class="col-9">
                            <br>
                            <input class="form-control" type="password" name="password" placeholder="Masukkan Password Anda !">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button name="LogInMember" class="btn btn-primary">Masuk</button>
                </form>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalDaftar" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-user-plus"></span> Daftar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <form action="query/query.php" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-4 text-right">
                                    <label for="" class="mt-2">Nama Lengkap</label>
                                    <br>
                                </div>
                                <div class="col-8">
                                    <input class="form-control" type="text" name="nama" value="" placeholder="Masukkan Email Anda ">
                                    <br>
                                </div>
                                <div class="col-4 text-right">
                                    <label for="" class="mt-2">Username</label>
                                </div>
                                <div class="col-8">
                                    <input class="form-control" type="text" name="username" value="" placeholder="Masukkan Username Anda ">
                                </div>
                                <div class="col-4 text-right">
                                    <br>
                                    <label for="" class="mt-2">Password </label>
                                </div>
                                <div class="col-8">
                                    <br>
                                    <input class="form-control" type="password" name="password" value="" placeholder="Masukkan Password Anda ">
                                </div>
                                <div class="col-4 text-right">
                                    <br>
                                    <label for="" class="mt-2">Pekerjaan </label>
                                </div>
                                <div class="col-8">
                                    <br>
                                    <input class="form-control" type="text" name="pekerjaan" value="" placeholder="Masukkan Pakerjaan Anda ">
                                    <br>
                                </div>
                                <div class="col-4 text-right">
                                    <label for="" class="mt-2">NoHP</label>
                                    <br>
                                </div>
                                <div class="col-8">
                                    <input class="form-control" type="text" name="nohp" value="" placeholder="Masukkan No HP Anda ">
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-4 text-right">
                                    <label for="" class="mt-2">Instagram</label>
                                </div>
                                <div class="col-8">
                                    <input class="form-control" type="text" name="ig" value="" placeholder="Masukkan Instagram Anda ">
                                </div>
                                <div class="col-4 text-right">
                                    <br>
                                    <label for="" class="mt-2">Alamat </label>
                                </div>
                                <div class="col-8">
                                    <br>
                                    <textarea name="alamat" cols="30" rows="6" class="form-control" placeholder="Masukkan Alamat Anda"></textarea>
                                </div>
                                <div class="col-4 text-right">
                                    <br>
                                    <label for="" class="mt-2">Foto Profile </label>
                                </div>
                                <div class="col-8">
                                    <br>
                                    <input class="form-control" type="file" name="foto">
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button name="DaftarMember" class="btn btn-primary">Daftar</button>
                </form>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalLogout" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalSayaLabel"><span class="fa fa-sign-out"></span> Konfirmasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Apakah Anda Ingin Sign Out ?</h5>
            </div>
            <div class="modal-footer">
                <a href="query/query.php?page=logout" class="btn btn-danger">Ya</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
            </div>
        </div>
    </div>
</div>